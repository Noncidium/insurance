package ru.mva.insurance.client;

import com.google.gwt.user.client.Window;
import com.google.gwt.validation.client.impl.Validation;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

public class BeanValidation {

    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public static <T> boolean isCorrectData (T data){
        Set<ConstraintViolation<T>> violations = validator.validate(data);
        if(violations.isEmpty()){
            return true;
        } else {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<T> violation : violations) {
                sb.append(violation.getMessage()).append("\n");
            }
            Window.alert(sb.toString());
            return false;
        }
    }

}
