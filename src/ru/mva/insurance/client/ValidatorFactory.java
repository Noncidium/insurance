package ru.mva.insurance.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.validation.client.AbstractGwtValidatorFactory;
import com.google.gwt.validation.client.GwtValidation;
import com.google.gwt.validation.client.impl.AbstractGwtValidator;
import ru.mva.insurance.shared.Client;
import ru.mva.insurance.shared.Contract;
import ru.mva.insurance.shared.RealEstate;

import javax.validation.Validator;

public class ValidatorFactory extends AbstractGwtValidatorFactory{

    @GwtValidation(value = {Client.class, Contract.class, RealEstate.class})
    public interface GwtValidator extends Validator {

    }

    @Override
    public AbstractGwtValidator createValidator() {
        return GWT.create(GwtValidator.class);
    }
}
