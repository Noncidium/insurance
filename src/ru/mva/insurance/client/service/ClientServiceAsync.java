package ru.mva.insurance.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.mva.insurance.shared.Client;

import java.util.List;

public interface ClientServiceAsync {

    void getAllClients(AsyncCallback<List<Client>> async);

    void saveClient(Client client, AsyncCallback<Client> async);

    void deleteClient(long id, AsyncCallback<Void> async);

    void get(long id, AsyncCallback<Client> async);

    void update(Client client, AsyncCallback<Void> async);
}
