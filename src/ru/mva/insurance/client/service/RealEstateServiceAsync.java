package ru.mva.insurance.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.mva.insurance.shared.RealEstate;
import java.util.List;

public interface RealEstateServiceAsync {

    void getAllRealEstate(AsyncCallback<List<RealEstate>> async);

    void saveRealEstate(RealEstate realEstate, AsyncCallback<RealEstate> async);

    void deleteRealEstate(long id, AsyncCallback<Void> async);

    void get(long id, AsyncCallback<RealEstate> async);
}
