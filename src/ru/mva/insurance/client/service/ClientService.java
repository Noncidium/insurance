package ru.mva.insurance.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import ru.mva.insurance.shared.Client;

import java.util.List;

@RemoteServiceRelativePath("ClientService")
public interface ClientService extends RemoteService {

    List<Client> getAllClients();
    Client saveClient(Client client);
    void deleteClient(long id);
    Client get(long id);
    void update(Client client);


    public static class App {
        private static final ClientServiceAsync ourInstance = (ClientServiceAsync) GWT.create(ClientService.class);

        public static ClientServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
