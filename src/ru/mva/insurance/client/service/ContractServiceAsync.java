package ru.mva.insurance.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.mva.insurance.shared.Client;
import ru.mva.insurance.shared.Contract;
import ru.mva.insurance.shared.RealEstate;

import java.util.List;

public interface ContractServiceAsync {

    void saveContract(Contract contract, Client client, RealEstate realEstate, AsyncCallback<Contract> async);

    void deleteContract(long id, AsyncCallback<Void> async);

    void getAllContracts(AsyncCallback<List<Contract>> async);

    void get(long id, AsyncCallback<Contract> async);

}
