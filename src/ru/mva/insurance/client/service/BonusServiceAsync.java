package ru.mva.insurance.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

public interface BonusServiceAsync {

    void getAllPropertyTypes(AsyncCallback<List<String>> async);

    void calculateValue(Double contractSum, Integer contractDays, String propertyType, Integer yearBuilt, Double area, AsyncCallback<Double> async);

}
