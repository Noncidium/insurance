package ru.mva.insurance.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("BonusService")
public interface BonusService extends RemoteService {

    List<String> getAllPropertyTypes();
    Double calculateValue(Double contractSum, Integer contractDays, String propertyType, Integer yearBuilt, Double area);

    public static class App {
        private static final BonusServiceAsync ourInstance = (BonusServiceAsync) GWT.create(BonusService.class);

        public static BonusServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
