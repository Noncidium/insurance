package ru.mva.insurance.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.mva.insurance.shared.RealEstate;

import java.util.List;

@RemoteServiceRelativePath("RealEstateService")
public interface RealEstateService extends RemoteService {

    List<RealEstate> getAllRealEstate();
    RealEstate saveRealEstate(RealEstate realEstate);
    void deleteRealEstate(long id);
    RealEstate get(long id);

    class App {
        private static final RealEstateServiceAsync ourInstance = (RealEstateServiceAsync) GWT.create(RealEstateService.class);
        public static RealEstateServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
