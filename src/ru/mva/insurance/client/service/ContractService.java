package ru.mva.insurance.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.mva.insurance.shared.Client;
import ru.mva.insurance.shared.Contract;
import ru.mva.insurance.shared.RealEstate;

import java.util.List;

@RemoteServiceRelativePath("ContractService")
public interface ContractService extends RemoteService {

    Contract saveContract (Contract contract, Client client, RealEstate realEstate);

    void deleteContract(long id);

    List<Contract> getAllContracts();

    Contract get(long id);

    public static class App {
        private static final ContractServiceAsync ourInstance = (ContractServiceAsync) GWT.create(ContractService.class);

        public static ContractServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
