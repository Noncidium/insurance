package ru.mva.insurance.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import ru.mva.insurance.client.BeanValidation;
import ru.mva.insurance.client.service.BonusService;
import ru.mva.insurance.client.service.BonusServiceAsync;
import ru.mva.insurance.client.service.ContractService;
import ru.mva.insurance.client.service.ContractServiceAsync;
import ru.mva.insurance.shared.Client;
import ru.mva.insurance.shared.Contract;
import ru.mva.insurance.shared.RealEstate;

import java.util.Date;
import java.util.List;

public class ContractForm extends Composite {

    private final java.lang.String onlyCountry = "Россия";
    private Insurance insuranceApp;
    private Widget root;
    private Contract currentContract;
    private Client chosenClient;
    private RealEstate currentRealEstate;

    interface ContractUiBinder extends UiBinder<Widget, ContractForm> {}
    private static ContractUiBinder uiBinder = GWT.create(ContractUiBinder.class);
    private BonusServiceAsync bonusService = BonusService.App.getInstance();
    private final long MILLISECONDS_IN_DAY = 1000 * 60 * 60 * 24;
    DateBox.Format dbFormat = new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd.MM.yyyy"));

    //Calculate fields
    @UiField
    TextBox contractCostTextBox;
    @UiField
    DateBox contractStartDate;
    @UiField
    DateBox contractEndDate;
    @UiField
    ListBox estateTypeListBox;
    @UiField
    TextBox builtYearTextBox;
    @UiField
    TextBox areaTextBox;
    @UiField
    Button calculateButton;
    @UiField
    DateBox calculateDateBox;
    @UiField
    TextBox bonusTextBox;
    @UiField
    TextBox contractNumberTextBox;
    @UiField
    DateBox contractCreateDate;
    //Clients Fields
    @UiField
    Button chooseClientButton;
    @UiField
    TextBox clientNameTextBox;
    @UiField
    Button changeClientButton;
    @UiField
    DateBox clientBirthDateBox;
    @UiField
    TextBox passportSeries;
    @UiField
    TextBox passportNumber;
    //Real estate fields
    @UiField
    ListBox countryListBox;
    @UiField
    TextBox zipCodeTextBox;
    @UiField
    TextBox regionTextBox;
    @UiField
    TextBox distinctTextBox;
    @UiField
    TextBox settlementTextBox;
    @UiField
    TextBox streetTextBox;
    @UiField
    TextBox houseTextBox;
    @UiField
    TextBox housingTextBox;
    @UiField
    TextBox buildingTextBox;
    @UiField
    TextBox apartmentTextBox;
    @UiField
    TextBox commentTextBox;
    @UiField
    Button saveButton;
    @UiField
    Button returnButton;

    public ContractForm(Contract contract) {
        root = uiBinder.createAndBindUi(this);
        this.currentContract = contract;

        setDataBoxesFormat(dbFormat);

        initializeWithDefaultValues();

        if(currentContract.getClient() != null && currentContract.getRealEstate() != null){
            setContract();
        }

        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //Initialize
                final Client client = chosenClient;
                final Contract contract = guiToContract();
                final RealEstate realEstate = guiToRealEstate();
                //Link together
                contract.setClient(client);
                contract.setRealEstate(realEstate);
                //Save
                if(isContractCorrect(contract) && isRealEstateIsCorrect(realEstate)){
                    saveContract(contract, client, realEstate);
                }
            }
        });

        returnButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                insuranceApp.showFrontPage();
            }
        });

        bonusService.getAllPropertyTypes(fillListBoxOnCallback());

        calculateButton.addClickHandler(getCalculateButtonHandler());

        chooseClientButton.addClickHandler(getClientButtonHandler());

        changeClientButton.addClickHandler(getChangeClientButtonHandler());

    }

    private void initializeWithDefaultValues() {
        countryListBox.clear();
        countryListBox.addItem(onlyCountry);
        contractStartDate.setValue(new Date());
        contractCreateDate.setValue(new Date());
    }

    private void setDataBoxesFormat(DateBox.Format format) {
        clientBirthDateBox.setFormat(format);
        contractCreateDate.setFormat(format);
        contractStartDate.setFormat(format);
        contractEndDate.setFormat(format);
        calculateDateBox.setFormat(format);
    }

    private AsyncCallback<List<String>> fillListBoxOnCallback() {
        return new AsyncCallback<List<String>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Возникла проблема при получении типов недвижимости");
            }
            @Override
            public void onSuccess(List<String> result) {
                int itemIndex = 0;
                for (String propertyType : result) {
                    estateTypeListBox.addItem(propertyType);
                    if (currentRealEstate != null && propertyType.equals(currentRealEstate.getPropertyType())) {
                        estateTypeListBox.setSelectedIndex(itemIndex);
                    }
                    itemIndex++;
                }
            }
        };
    }

    private ClickHandler getCalculateButtonHandler() {
        return new ClickHandler() {
            public void onClick(ClickEvent event) {
                Boolean infoIsCorrect = (!contractCostTextBox.getValue().isEmpty() &&
                        contractStartDate.getValue() != null &&
                        contractEndDate.getValue() != null &&
                        !estateTypeListBox.getSelectedValue().isEmpty() &&
                        !builtYearTextBox.getValue().isEmpty() &&
                        !areaTextBox.getValue().isEmpty()
                );
                if (!infoIsCorrect)  {
                    Window.alert("Не заполнены все поля необходимые для расчета премии");
                } else if (!isCorrectContractDayDifference(contractStartDate.getValue(), contractEndDate.getValue())
                        || !isBuiltYearIsCorrect(builtYearTextBox.getText())){
                    Window.alert("Некорректно заполнены сроки договора");
                } else if (!isAreaIsCorrect(areaTextBox.getValue())) {
                    Window.alert("Некорректно введена площадь");
                } else {
                    Integer diffInDays = (int)( (contractEndDate.getValue().getTime() - contractStartDate.getValue().getTime())
                            / (MILLISECONDS_IN_DAY) );
                    bonusService.calculateValue(
                            Double.valueOf(contractCostTextBox.getValue()),
                            diffInDays,
                            estateTypeListBox.getSelectedValue(),
                            Integer.valueOf(builtYearTextBox.getValue()),
                            Double.valueOf(areaTextBox.getValue()),
                            new AsyncCallback<Double>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    Window.alert("Возникла проблема при расчете премии");
                                }
                                @Override
                                public void onSuccess(Double result) {
                                    bonusTextBox.setValue(String.valueOf(result));
                                    calculateDateBox.setValue(new Date());
                                }
                            }
                    );
                }
            }
        };
    }

    private ClickHandler getClientButtonHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ClientSelection clientSelection = new ClientSelection();
                clientSelection.setContractForm(ContractForm.this);
                clientSelection.show();
            }
        };
    }

    private ClickHandler getChangeClientButtonHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ClientChange clientChange = new ClientChange();
                clientChange.setClientSelection(ContractForm.this, chosenClient);
                clientChange.show();
            }
        };
    }

    public Widget getContractForm() {
        return root;
    }

    public void setInsuranceApp(Insurance insuranceApp) {
        this.insuranceApp = insuranceApp;
    }

    public Contract guiToContract() {
        final Contract contract;
        if (currentContract != null) {
            contract = currentContract;
        } else {
            contract = new Contract();
        }
        contract.setContractCost(contractCostTextBox.getValue());
        contract.setContractNumber(Integer.valueOf(contractNumberTextBox.getText()));
        contract.setComment(commentTextBox.getValue());
        contract.setCrateDate(contractCreateDate.getValue());
        contract.setStartDate(contractStartDate.getValue());
        contract.setEndDate(contractEndDate.getValue());
        contract.setCalculateDate(calculateDateBox.getValue());
        contract.setBonus(Double.parseDouble(bonusTextBox.getValue()));
        return contract;
    }

    private RealEstate guiToRealEstate() {
        final RealEstate realEstate;
        if (currentRealEstate != null) {
            realEstate = currentRealEstate;
        } else {
            realEstate = new RealEstate();
        }
        realEstate.setPropertyType(estateTypeListBox.getSelectedValue());
        realEstate.setCountry(onlyCountry);
        realEstate.setZipCode(zipCodeTextBox.getValue());
        realEstate.setRegion(regionTextBox.getValue());
        realEstate.setDistrict(distinctTextBox.getValue());
        realEstate.setSettlement(settlementTextBox.getValue());
        realEstate.setStreet(streetTextBox.getValue());
        realEstate.setHouse(houseTextBox.getValue());
        realEstate.setHousing(housingTextBox.getValue());
        realEstate.setBuilding(buildingTextBox.getValue());
        realEstate.setApartment(apartmentTextBox.getValue());
        realEstate.setYearBuilt(Integer.valueOf(builtYearTextBox.getValue()));
        realEstate.setArea(Double.valueOf(areaTextBox.getValue()));
        return realEstate;
    }



    public void saveContract(final Contract contract, final Client client, final RealEstate realEstate) {
        ContractServiceAsync contractService = ContractService.App.getInstance();
        contractService.getAllContracts(new AsyncCallback<List<Contract>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Возникла проблема при проверке номера договора");
            }
            @Override
            public void onSuccess(List<Contract> result) {
                if (isContractExists(result, contract)) {
                    Window.alert("Такой номер договора уже существует. Договор не сохранен.");
                } else {
                    ContractServiceAsync serviceAsync = ContractService.App.getInstance();
                    serviceAsync.saveContract(contract,client,realEstate, new AsyncCallback<Contract>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            Window.alert("Возникла проблема при сохранении договора");
                        }
                        @Override
                        public void onSuccess(Contract result) {
                            Window.alert("Договор успешно сохранен");
                        }
                    });
                }
            }
        });
    }

    private boolean isContractExists(List<Contract> existingContracts, Contract contract) {
        boolean contractExists = false;
        for (Contract existingContract : existingContracts) {
            boolean sameObjects = (existingContract.getId() == contract.getId());
            boolean equalContactNumbers = (existingContract.getContractNumber() == contract.getContractNumber());
            if (!sameObjects && equalContactNumbers) {
                contractExists = true;
            }
        }
        return contractExists;
    }

    public void setContract(){
        Client client = currentContract.getClient();
        RealEstate realEstate = currentContract.getRealEstate();
        //contract fields
        contractCostTextBox.setText(currentContract.getContractCost());
        contractNumberTextBox.setText(currentContract.getContractNumberFormatted());
        commentTextBox.setText(currentContract.getComment());
        contractCreateDate.setValue(currentContract.getCrateDate());
        contractStartDate.setValue(currentContract.getStartDate());
        contractEndDate.setValue(currentContract.getEndDate());
        calculateDateBox.setValue(currentContract.getCalculateDate());
        bonusTextBox.setValue(String.valueOf(currentContract.getBonus()));

        //set Client fields
        clientNameTextBox.setText(client.toString());
        clientBirthDateBox.setValue(client.getBirthDay());
        passportNumber.setText(String.valueOf(client.getPassportNumber()));
        passportSeries.setText(String.valueOf(client.getPassportSeries()));

        //set RealEstate fields
        countryListBox.addItem(realEstate.getCountry());
        zipCodeTextBox.setText(realEstate.getZipCode());
        regionTextBox.setText(realEstate.getRegion());
        distinctTextBox.setText(realEstate.getDistrict());
        settlementTextBox.setText(realEstate.getSettlement());
        streetTextBox.setText(realEstate.getStreet());
        houseTextBox.setText(realEstate.getHouse());
        housingTextBox.setText(realEstate.getHousing());
        buildingTextBox.setText(realEstate.getBuilding());
        apartmentTextBox.setText(realEstate.getApartment());
        builtYearTextBox.setText(String.valueOf(realEstate.getYearBuilt()));
        areaTextBox.setText(String.valueOf(realEstate.getArea()));
        this.currentRealEstate = realEstate;

        //set Client Fields
        setChosenClient(currentContract.getClient());
    }

    public void setChosenClient(Client chosenClient) {
        this.chosenClient = chosenClient;
        clientNameTextBox.setValue(chosenClient.toString());
        passportNumber.setText(chosenClient.getPassportNumber());
        passportSeries.setText(chosenClient.getPassportSeries());
        clientBirthDateBox.setValue(chosenClient.getBirthDay());
    }

    private boolean isCorrectContractDayDifference(Date startDate, Date endDate){
        //Difference between endDate and startDate should be > 0
        //Difference between startDate and endDate should be <= 365 days
        long diff = endDate.getTime() - startDate.getTime();
        long diffDays = diff / (MILLISECONDS_IN_DAY);
        return diff > 0 && diffDays <= 365;
    }

    private boolean isContractCorrect(Contract contract){
        return BeanValidation.isCorrectData(contract);
    }

    private boolean isRealEstateIsCorrect(RealEstate realEstate){
       return BeanValidation.isCorrectData(realEstate);
    }

    private boolean isBuiltYearIsCorrect(String year){
        try{
            int length = year.length();
            if(length == 4){
                return true;
            }
        } catch (NumberFormatException e){
            return false;
        }
        return false;
    }

    private boolean isAreaIsCorrect(String area){
        if (area.contains(",")){
            return false;
        } else if (!area.contains(".")) {
           return true;
        } else {
            int integerPlaces = area.indexOf('.');
            int decimalPlaces = area.length() - integerPlaces - 1;
            if (decimalPlaces == 1){
                return true;
            }

        }
        return false;
    }


}