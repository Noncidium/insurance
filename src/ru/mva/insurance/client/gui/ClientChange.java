package ru.mva.insurance.client.gui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import ru.mva.insurance.client.BeanValidation;
import ru.mva.insurance.client.service.ClientService;
import ru.mva.insurance.shared.Client;

public class ClientChange extends PopupPanel {

    private final TextBox lastNameTextBox = new TextBox();
    private final TextBox firstNameTextBox = new TextBox();
    private final TextBox middleNameTextBox = new TextBox();

    private DateBox dateBox = new DateBox();

    private final TextBox passportSeriesTextBox = new TextBox();
    private final TextBox passportNumberTextBox = new TextBox();

    private Client clientToChange;
    private ContractForm contractForm;


    ClientChange() {
        super(true);

        HorizontalPanel clientNamePanel = new HorizontalPanel();
        clientNamePanel.add(new Label("ФИО:"));
        clientNamePanel.add(lastNameTextBox);
        clientNamePanel.add(firstNameTextBox);
        clientNamePanel.add(middleNameTextBox);

        Panel datePanel = new HorizontalPanel();
        dateBox.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd.MM.yyyy")));
        datePanel.add(new Label("Дата:"));
        datePanel.add(dateBox);

        Panel passportPanel = new HorizontalPanel();
        passportPanel.add(new Label("Серия:"));
        passportSeriesTextBox.setWidth("162");
        passportPanel.add(passportSeriesTextBox);
        passportPanel.add(new Label("№:"));
        passportPanel.add(passportNumberTextBox);

        Panel buttonPanel = new HorizontalPanel();
        buttonPanel.add(getSaveButton());
        buttonPanel.add(getCancelButton());

        VerticalPanel mainPanel = new VerticalPanel();
        mainPanel.add(clientNamePanel);
        mainPanel.add(datePanel);
        mainPanel.add(passportPanel);
        mainPanel.add(buttonPanel);

        setWidget(mainPanel);
        Popup.setPopupPosition(this);
    }

    public void setClientSelection(ContractForm contractForm, Client selectedClient) {
        this.contractForm = contractForm;
        this.clientToChange = selectedClient;
        initialize();
    }

    public void initialize() {
        lastNameTextBox.setValue(clientToChange.getLastName());
        firstNameTextBox.setValue(clientToChange.getFirstName());
        middleNameTextBox.setValue(clientToChange.getMiddleName());
        dateBox.setValue(clientToChange.getBirthDay());
        String passportSeries = clientToChange.getPassportSeries() == null ? "" : clientToChange.getPassportSeries();
        String passportNumber = clientToChange.getPassportNumber() == null ? "" : clientToChange.getPassportNumber();
        passportSeriesTextBox.setValue(passportSeries);
        passportNumberTextBox.setValue(passportNumber);
    }

    private Button getSaveButton() {
        return new Button("Сохранить", new ClickHandler() {
            public void onClick(ClickEvent event) {
                Boolean clientInfoIsCorrect = (
                        !firstNameTextBox.getValue().isEmpty() &&
                        !lastNameTextBox.getValue().isEmpty() &&
                        dateBox.getValue() != null &&
                        !passportSeriesTextBox.getValue().isEmpty() &&
                        !passportNumberTextBox.getValue().isEmpty()
                );
                if (!clientInfoIsCorrect) {
                    Window.alert("Данные о клиенте заполнены не полностью");
                } else {
                    clientToChange.setLastName(lastNameTextBox.getValue());
                    clientToChange.setFirstName(firstNameTextBox.getValue());
                    clientToChange.setMiddleName(middleNameTextBox.getValue());
                    clientToChange.setBirthDay(dateBox.getValue());
                    clientToChange.setPassportSeries(passportSeriesTextBox.getValue());
                    clientToChange.setPassportNumber(passportNumberTextBox.getValue());
                    if(isClientCorrect(clientToChange)){
                        ClientService.App.getInstance().saveClient(clientToChange, new AsyncCallback<Client>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                Window.alert("Возникла проблема при изменении клиента");
                            }

                            @Override
                            public void onSuccess(Client result) {
                                contractForm.setChosenClient(result);
                                ClientChange.this.clear();
                            }
                        });
                    }
                }
            }
        });
    }

    private Button getCancelButton() {
        return new Button("Отменить", new ClickHandler() {
            public void onClick(ClickEvent event) {
                ClientChange.this.clear();
            }
        });
    }

    private boolean isClientCorrect(Client client){
        return BeanValidation.isCorrectData(client);
    }


}
