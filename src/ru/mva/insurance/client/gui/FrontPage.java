package ru.mva.insurance.client.gui;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import ru.mva.insurance.client.service.ContractService;
import ru.mva.insurance.client.service.ContractServiceAsync;
import ru.mva.insurance.shared.Contract;

import java.util.Date;
import java.util.List;

public class FrontPage {

    private FlowPanel flowPanel = new FlowPanel();
    final CellTable<Contract> table = new CellTable<>();
    ListDataProvider<Contract> dataProvider = new ListDataProvider<>();
    final List<Contract> list = dataProvider.getList();
    ContractServiceAsync contractService = ContractService.App.getInstance();
    final SingleSelectionModel<Contract> contactSingleSelectMode = new SingleSelectionModel<>();

    private Contract selectedContract;
    private Insurance insuranceApp;

    public FrontPage() {

        //buttons
        HorizontalPanel buttonPanel = new HorizontalPanel();
        Button createContractButton = new Button("Создать договор", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                insuranceApp.showEditContractDialog(new Contract());
            }
        });
        buttonPanel.add(createContractButton);
        Button openContractButton = new Button("Открыть договор", new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if(selectedContract != null){
                    insuranceApp.showEditContractDialog(selectedContract);
                }
            }
        });
        buttonPanel.add(openContractButton);
        buttonPanel.getElement().getStyle().setPadding(20, Style.Unit.PX);
        flowPanel.add(buttonPanel);

        //table
        flowPanel.add(table);
        TextColumn<Contract> contractNumber = new TextColumn<Contract>() {
            @Override
            public String getValue(Contract contract) {
                return contract.getContractNumberFormatted();
            }
        };
        table.addColumn(contractNumber,"Серия-Номер");


        DateCell dateCell = new DateCell(DateTimeFormat.getFormat("dd.MM.yyyy"));
        Column<Contract, Date> startDate= new Column<Contract, Date>(dateCell) {
            @Override
            public Date getValue(Contract contract) {
                return contract.getStartDate();
            }
        };
        table.addColumn(startDate,"Дата заключения");


        TextColumn<Contract> client = new TextColumn<Contract>() {
            @Override
            public String getValue(Contract contract) {
                return contract.getClient().toString();
            }
        };
        table.addColumn(client,"Клиент");

        TextColumn<Contract> bonus = new TextColumn<Contract>() {
            @Override
            public String getValue(Contract contract) {
                return String.valueOf(contract.getBonus());
            }
        };
        table.addColumn(bonus,"Премия");

        TextColumn<Contract> period = new TextColumn<Contract>() {
            @Override
            public String getValue(Contract contract) {
                return contract.getPeriod();
            }
        };
        table.addColumn(period,"Срок действия");

        dataProvider.addDataDisplay(table);

        contractService.getAllContracts(new AsyncCallback<List<Contract>>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(List<Contract> result) {
                for (Contract contract : result) {
                    list.add(contract);
                }
            }
        });

        //select listener
        table.setSelectionModel(contactSingleSelectMode);
        contactSingleSelectMode.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                Contract selectedContract = contactSingleSelectMode.getSelectedObject();
                if (selectedContract != null) {
                    setCurrentContract(selectedContract);
                }
            }
        });
    }

    public FlowPanel getFrontPage() {
        return flowPanel;
    }

    private void setCurrentContract(Contract contract){
        this.selectedContract = contract;
    }


    public void setInsuranceApp(Insurance insuranceApp) {
        this.insuranceApp = insuranceApp;
    }
}
