package ru.mva.insurance.client.gui;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import ru.mva.insurance.client.service.ClientService;
import ru.mva.insurance.client.service.ClientServiceAsync;
import ru.mva.insurance.shared.Client;

import java.util.Date;
import java.util.List;

public class ClientSelection extends PopupPanel {

    private final TextBox lastNameTextBox = new TextBox();
    private final TextBox firstNameTextBox = new TextBox();
    private final TextBox middleNameTextBox = new TextBox();

    private CellTable<Client> clientsTable = new CellTable<>();
    Client selectedClientInTable;
    ListDataProvider<Client> dataProvider = new ListDataProvider<>();
    final List<Client> list = dataProvider.getList();
    ClientServiceAsync clientService = ClientService.App.getInstance();
    private ContractForm contractForm;

    ClientSelection() {
        super(true);

        Panel clientNamePanel = new HorizontalPanel();
        clientNamePanel.add(new Label("ФИО:"));
        clientNamePanel.add(lastNameTextBox);
        clientNamePanel.add(firstNameTextBox);
        clientNamePanel.add(middleNameTextBox);
        clientNamePanel.add(getSearchClientsButton());

        Panel buttonPanel = new HorizontalPanel();
        buttonPanel.add(getSelectClientButton());
        buttonPanel.add(getNewClientButton());
        buttonPanel.add(getClosePopupButton());

        VerticalPanel mainPanel = new VerticalPanel();
        mainPanel.add(clientNamePanel);
        clientsTable = getClientsTable();
        mainPanel.add(clientsTable);
        mainPanel.add(buttonPanel);

        setWidget(mainPanel);
        Popup.setPopupPosition(this);
    }

    private CellTable<Client> getClientsTable() {
        final CellTable<Client> table = new CellTable<>();
        TextColumn<Client> contractNumber = new TextColumn<Client>() {
            @Override
            public String getValue(Client client) {
                return client.toString();
            }
        };
        table.addColumn(contractNumber,"ФИО");

        DateCell dateCell = new DateCell(com.google.gwt.i18n.shared.DateTimeFormat.getFormat("dd.MM.yyyy"));
        Column<Client, Date> startDate= new Column<Client, Date>(dateCell) {
            @Override
            public Date getValue(Client client) {
                return client.getBirthDay();
            }
        };
        table.addColumn(startDate,"Дата рождения");

        TextColumn<Client> client = new TextColumn<Client>() {
            @Override
            public String getValue(Client client) {
                String passportSeries = client.getPassportSeries() == null ? "" : String.valueOf(client.getPassportSeries());
                String passportNumber = client.getPassportNumber() == null ? "" : String.valueOf(client.getPassportNumber());
                return String.valueOf(passportSeries + "-" + passportNumber);
            }
        };
        table.addColumn(client,"Паспортные данные");

        clientService.getAllClients(new AsyncCallback<List<Client>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Возникла проблема при заполнении таблицы с клиентами");
            }

            @Override
            public void onSuccess(List<Client> result) {
                for (Client client : result) {
                    list.add(client);
                }
            }
        });

        final NoSelectionModel<Client> selectionModel = new NoSelectionModel<>();
        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                selectedClientInTable = selectionModel.getLastSelectedObject();
            }
        });
        table.setSelectionModel(selectionModel);

        dataProvider.addDataDisplay(table);

        return table;
    }

    private Button getSearchClientsButton() {
        return new Button("Поиск", new ClickHandler() {
            public void onClick(ClickEvent event) {
                searchClients();
            }
        });
    }

    public void searchClients() {
        final String lastNameRegex = (".*" + lastNameTextBox.getValue().trim() + ".*").toLowerCase();
        final String firstNameRegex = (".*" + firstNameTextBox.getValue().trim() + ".*").toLowerCase();
        final String middleNameRegex = (".*" + middleNameTextBox.getValue().trim() + ".*").toLowerCase();

        clientService.getAllClients(new AsyncCallback<List<Client>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Возникла проблема при поиске клиентов");
            }

            @Override
            public void onSuccess(List<Client> result) {
                list.clear();
                for (Client client : result) {
                    String lastNameFormatted = client.getLastName().toLowerCase();
                    String firstNameFormatted = client.getFirstName().toLowerCase();
                    String middleNameFormatted = client.getMiddleName().toLowerCase();
                    if (    lastNameFormatted.matches(lastNameRegex) &&
                            firstNameFormatted.matches(firstNameRegex) &&
                            middleNameFormatted.matches(middleNameRegex)) {
                        list.add(client);
                    }
                }
            }
        });
    }

    private Button getSelectClientButton() {
        return new Button("Выбрать", new ClickHandler() {
            public void onClick(ClickEvent event) {
                contractForm.setChosenClient(selectedClientInTable);
                ClientSelection.this.clear();
            }
        });
    }

    private Button getNewClientButton() {
        return new Button("Новый", new ClickHandler() {
            public void onClick(ClickEvent event) {
                ClientAdd clientAdd = new ClientAdd();
                clientAdd.setClientSelection(contractForm, ClientSelection.this);
                clientAdd.show();
            }
        });
    }


    private Button getClosePopupButton() {
        return new Button("Закрыть", new ClickHandler() {
            public void onClick(ClickEvent event) {
                ClientSelection.this.clear();
            }
        });
    }

    public void setContractForm(ContractForm contractForm) {
        this.contractForm = contractForm;
    }
}
