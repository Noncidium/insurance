package ru.mva.insurance.client.gui;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import ru.mva.insurance.shared.Contract;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class Insurance implements EntryPoint {

    public void onModuleLoad() {
        showFrontPage();
    }

    public void showFrontPage(){
        FrontPage fp = new FrontPage();
        fp.setInsuranceApp(this);
        RootPanel.get("slot1").clear();
        RootPanel.get("slot1").add(fp.getFrontPage());
    }

    public void showEditContractDialog(Contract contract){
        ContractForm contractForm = new ContractForm(contract);
        contractForm.setInsuranceApp(this);
        RootPanel.get("slot1").clear();
        RootPanel.get("slot1").add(contractForm.getContractForm());
    }
}
