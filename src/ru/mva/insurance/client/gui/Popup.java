package ru.mva.insurance.client.gui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;

public class Popup {

    public static void setPopupPosition(final PopupPanel popupPanel) {
        popupPanel.setPopupPositionAndShow(new PopupPanel.PositionCallback() {
            public void setPosition(int offsetWidth, int offsetHeight) {
                int leftPosition = (Window.getClientWidth() - offsetWidth) / 2;
                int topPosition = (Window.getClientHeight() - offsetHeight) / 4;
                popupPanel.setPopupPosition(leftPosition, topPosition);
            }
        });
    }

}
