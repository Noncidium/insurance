package ru.mva.insurance.client.gui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import ru.mva.insurance.client.BeanValidation;
import ru.mva.insurance.client.service.ClientService;
import ru.mva.insurance.shared.Client;

public class ClientAdd extends PopupPanel {

    private final TextBox lastNameTextBox = new TextBox();
    private final TextBox firstNameTextBox = new TextBox();
    private final TextBox middleNameTextBox = new TextBox();

    private DateBox dateBox = new DateBox();
    private ClientSelection clientSelection;
    private ContractForm contractForm;

    ClientAdd() {
        super(true);

        HorizontalPanel clientNamePanel = new HorizontalPanel();
        clientNamePanel.add(new Label("ФИО:"));
        clientNamePanel.add(lastNameTextBox);
        clientNamePanel.add(firstNameTextBox);
        clientNamePanel.add(middleNameTextBox);

        Panel datePanel = new HorizontalPanel();
        dateBox.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd.MM.yyyy")));
        datePanel.add(new Label("Дата:"));
        datePanel.add(dateBox);

        Panel buttonPanel = new HorizontalPanel();
        buttonPanel.add(getSaveButton());
        buttonPanel.add(getCancelButton());

        VerticalPanel mainPanel = new VerticalPanel();
        mainPanel.add(clientNamePanel);
        mainPanel.add(datePanel);
        mainPanel.add(buttonPanel);

        setWidget(mainPanel);
        Popup.setPopupPosition(this);
    }

    public void setClientSelection(ContractForm contractForm, ClientSelection clientSelection) {
        this.contractForm = contractForm;
        this.clientSelection = clientSelection;
    }

    private Button getSaveButton() {
        return new Button("Сохранить", new ClickHandler() {
            public void onClick(ClickEvent event) {
                Boolean clientInfoIsCorrect = (
                        !firstNameTextBox.getValue().isEmpty() &&
                        !lastNameTextBox.getValue().isEmpty() &&
                        dateBox.getValue() != null
                );
                if (!clientInfoIsCorrect) {
                    Window.alert("Данные о клиенте заполнены не полностью");
                } else {
                    Client client = new Client();
                    client.setLastName(lastNameTextBox.getValue());
                    client.setFirstName(firstNameTextBox.getValue());
                    client.setMiddleName(middleNameTextBox.getValue());
                    client.setBirthDay(dateBox.getValue());

                    if(isClientCorrect(client)){
                        ClientService.App.getInstance().saveClient(client, new AsyncCallback<Client>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                Window.alert("Возникла проблема при сохранении клиента");
                            }

                            @Override
                            public void onSuccess(Client result) {
                                ClientAdd.this.clear();
                                clientSelection.clear();
                                contractForm.setChosenClient(result);
                            }
                        });
                    }
                }
            }
        });
    }

    private Button getCancelButton() {
        return new Button("Отменить", new ClickHandler() {
            public void onClick(ClickEvent event) {
                ClientAdd.this.clear();
            }
        });
    }

    private boolean isClientCorrect(Client client){
        return BeanValidation.isCorrectData(client);
    }
}
