package ru.mva.insurance.server.db;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.mva.insurance.client.service.ClientService;
import ru.mva.insurance.shared.Client;
import sk.nociar.jpacloner.JpaCloner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ClientServiceImpl extends RemoteServiceServlet implements ClientService {


    public ClientServiceImpl() {

    }

    @Override
    public Client saveClient (final Client client) {
        return QueryProvider.doQueryInTransaction(new QueryProvider.Query<Client>() {
            @Override
            public Client call(EntityManager manager) {
                Client fromDB = manager.merge(client);
                return JpaCloner.clone(fromDB);
            }
        });
    }

    @Override
    public void deleteClient(final long id) {
        QueryProvider.doQueryInTransaction(new QueryProvider.Query<Void>() {
            @Override
            public Void call(EntityManager manager) {
                manager.remove(manager.find(Client.class, id));
                return null;
            }
        });
    }

    @Override
    public void update(final Client client){
        QueryProvider.doQueryInTransaction(new QueryProvider.Query<Void>() {
            @Override
            public Void call(EntityManager manager) {
                manager.merge(client);
                return null;
            }
        });
    }

    @Override
    public List<Client> getAllClients() {
        return QueryProvider.doQueryInTransaction(new QueryProvider.Query<List<Client>>() {
            @Override
            public List<Client> call(EntityManager manager) {
                TypedQuery<Client> namedQuery = manager.createNamedQuery("Client.getAll", Client.class);
                List<Client> clients = namedQuery.getResultList();
                return JpaCloner.clone(clients);
            }
        });
    }

    @Override
    public Client get(final long id) {
        return QueryProvider.doQueryCasual(new QueryProvider.Query<Client>() {
            @Override
            public Client call(EntityManager manager) {
                Client fromDB = manager.find(Client.class, id);
                return JpaCloner.clone(fromDB);
            }
        });
    }



}