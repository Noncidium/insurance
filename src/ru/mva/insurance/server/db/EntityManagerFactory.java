package ru.mva.insurance.server.db;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EntityManagerFactory {

    private EntityManagerFactory() {}

    private static javax.persistence.EntityManagerFactory entityManagerFactory;

    static {
        entityManagerFactory = Persistence.createEntityManagerFactory("InterviewAppLOIS");
    }

    public static EntityManager createEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    public static void closeEntityManagerFactory(){
        entityManagerFactory.close();
    }
}
