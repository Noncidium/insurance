package ru.mva.insurance.server.db;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.mva.insurance.client.service.ContractService;
import ru.mva.insurance.server.logic.Formatter;
import ru.mva.insurance.shared.Client;
import ru.mva.insurance.shared.Contract;
import ru.mva.insurance.shared.RealEstate;
import sk.nociar.jpacloner.JpaCloner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ContractServiceImpl extends RemoteServiceServlet implements ContractService {

    public ContractServiceImpl() {
    }

    @Override
    public Contract saveContract(final Contract contract, final Client client, final RealEstate realEstate) {
        return QueryProvider.doQueryInTransaction(new QueryProvider.Query<Contract>() {
            @Override
            public Contract call(EntityManager manager) {
                Client clientFromDB = manager.merge(client);
                RealEstate realEstateFromDB = manager.merge(realEstate);
                contract.setContractNumberFormatted(Formatter.contractNumberToString(contract.getContractNumber()));
                contract.setClient(clientFromDB);
                contract.setRealEstate(realEstateFromDB);
                contract.setPeriod(Formatter.periodToString(contract.getStartDate(),contract.getStartDate()));
                Contract fromDB = manager.merge(contract);
                return JpaCloner.clone(fromDB);
            }
        });
    }

    @Override
    public void deleteContract(final long id) {
        QueryProvider.doQueryInTransaction(new QueryProvider.Query<Void>() {
            @Override
            public Void call(EntityManager manager) {
                manager.remove(manager.find(Contract.class, id));
                return null;
            }
        });
    }

    @Override
    public List<Contract> getAllContracts() {
        return QueryProvider.doQueryInTransaction(new QueryProvider.Query<List<Contract>>() {
            @Override
            public List<Contract> call(EntityManager manager) {
                TypedQuery<Contract> namedQuery = manager.createNamedQuery("Contract.getAll", Contract.class);
                List<Contract> contracts = namedQuery.getResultList();
                return JpaCloner.clone(contracts, "*");
            }
        });
    }

    @Override
    public Contract get(final long id) {
        return QueryProvider.doQueryCasual(new QueryProvider.Query<Contract>() {
            @Override
            public Contract call(EntityManager manager) {
                Contract fromDB = manager.find(Contract.class, id);
                return JpaCloner.clone(fromDB);
            }
        });
    }

}