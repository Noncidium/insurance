package ru.mva.insurance.server.db;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.mva.insurance.client.service.RealEstateService;
import ru.mva.insurance.shared.RealEstate;
import sk.nociar.jpacloner.JpaCloner;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class RealEstateServiceImpl extends RemoteServiceServlet implements RealEstateService {

    public RealEstateServiceImpl() {

    }

    @Override
    public RealEstate saveRealEstate (final RealEstate realEstate) {
        return QueryProvider.doQueryInTransaction(new QueryProvider.Query<RealEstate>() {
            @Override
            public RealEstate call(EntityManager manager) {
                RealEstate fromDB = manager.merge(realEstate);
                return JpaCloner.clone(fromDB);
            }
        });
    }

    @Override
    public void deleteRealEstate(final long id) {
        QueryProvider.doQueryInTransaction(new QueryProvider.Query<Void>() {
            @Override
            public Void call(EntityManager manager) {
                manager.remove(manager.find(RealEstate.class, id));
                return null;
            }
        });
    }

    @Override
    public List<RealEstate> getAllRealEstate() {
        return QueryProvider.doQueryInTransaction(new QueryProvider.Query<List<RealEstate>>() {
            @Override
            public List<RealEstate> call(EntityManager manager) {
                TypedQuery<RealEstate> namedQuery = manager.createNamedQuery("RealEstate.getAll", RealEstate.class);
                List usersList = namedQuery.getResultList();
                return JpaCloner.clone(usersList);
            }
        });
    }

    @Override
    public RealEstate get(final long id) {
        return QueryProvider.doQueryCasual(new QueryProvider.Query<RealEstate>() {
            @Override
            public RealEstate call(EntityManager manager) {
                RealEstate fromDB = manager.find(RealEstate.class,id);
                return JpaCloner.clone(fromDB);
            }
        });

    }
}