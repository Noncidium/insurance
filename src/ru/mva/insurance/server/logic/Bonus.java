package ru.mva.insurance.server.logic;


import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.mva.insurance.client.service.BonusService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bonus extends RemoteServiceServlet implements BonusService {

    /* */
    private static Map<String, Double> propertyTypeCoefficients;

    static {
        propertyTypeCoefficients = new HashMap<>();
        propertyTypeCoefficients.put("Квартира", 1.7);
        propertyTypeCoefficients.put("Дом", 1.5);
        propertyTypeCoefficients.put("Комната", 1.3);
    }

    public List<String> getAllPropertyTypes() {
        List<String> propertyTypes = new ArrayList<>();
        for (String propertyType : propertyTypeCoefficients.keySet()) {
            propertyTypes.add(propertyType);
        }
        return propertyTypes;
    }

    private static Double getPropertyTypeCoefficient(String propertyType) {
        return propertyTypeCoefficients.get(propertyType);
    }

    /* */
    private static Double getYearBuiltCoefficient(Integer yearBuilt) {
        Double coefficient = null;
        if (yearBuilt < 2000) {
            coefficient = 1.3;
        }
        if (yearBuilt >= 2000 && yearBuilt <= 2014) {
            coefficient = 1.6;
        }
        if (yearBuilt > 2014) {
            coefficient = 2.0;
        }
        return  coefficient;
    }

    /* */
    private static Double getAreaCoefficient(double area) {
        Double coefficient = null;
        if (area < 50) {
            coefficient = 1.2;
        }
        if (area >= 50 && area <= 100) {
            coefficient = 1.5;
        }
        if (area > 100) {
            coefficient = 2.0;
        }
        return  coefficient;
    }

    /* */
    public Double calculateValue(Double contractSum, Integer contractDays, String propertyType, Integer yearBuilt, Double area) {
        Double propertyTypeCoefficient = Bonus.getPropertyTypeCoefficient(propertyType);
        Double yearBuiltCoefficient = Bonus.getYearBuiltCoefficient(yearBuilt);
        Double areaCoefficient = Bonus.getAreaCoefficient(area);
        double result = (contractSum / contractDays) * propertyTypeCoefficient * yearBuiltCoefficient * areaCoefficient;
        return (double) Math.round(result * 100) / 100;
    }

}
