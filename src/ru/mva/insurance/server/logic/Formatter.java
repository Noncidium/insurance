package ru.mva.insurance.server.logic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {

    public static String contractNumberToString(int number){
        return String.format("%06d", number);
    }

    public static String periodToString(Date startDate, Date endDate){
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        return formatter.format(startDate) + "-" + formatter.format(endDate);
    }

}
