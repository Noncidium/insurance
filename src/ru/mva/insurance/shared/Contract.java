package ru.mva.insurance.shared;

import org.hibernate.validator.constraints.NotEmpty;
import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name="contracts")
@NamedQueries({
        @NamedQuery(name="Contract.getAll", query="SELECT c FROM Contract c")
})
public class Contract extends Model{

    @NotNull(message = "Номер договора обязателен к заполнению")
    @Column(unique = true)
    private Integer contractNumber;
    private Date crateDate;
    @Future(message = "Поле срок действия с должен быть не ранее сегодняшней даты")
    private Date startDate;
    private Date endDate;
    private Date calculateDate;
    private String period;
    private double bonus;
    private String contractNumberFormatted;
    private String comment;
    @NotEmpty(message = "Поле страховая сумма обязательно к заполнению")
    private String contractCost;

    public Contract() {
    }

    @OneToOne
    @JoinColumn(name="client_id")
    private Client client;

    @OneToOne
    @JoinColumn(name="real_estate_id")
    private RealEstate realEstate;

    public Client getClient() {
        return client;
    }
    public void setClient(Client oneToOne) {
        this.client = oneToOne;
    }
    public Integer getContractNumber() {
        return contractNumber;
    }
    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public RealEstate getRealEstate() {
        return realEstate;
    }
    public void setRealEstate(RealEstate realEstate) {
        this.realEstate = realEstate;
    }
    public String getPeriod() {
        return period;
    }
    public void setPeriod(String period) {
        this.period = period;
    }
    public double getBonus() {
        return bonus;
    }
    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    public String getContractNumberFormatted() {
        return contractNumberFormatted;
    }
    public void setContractNumberFormatted(String contractNumberFormatted) {
        this.contractNumberFormatted = contractNumberFormatted;
    }
    public Date getCrateDate() {
        return crateDate;
    }
    public void setCrateDate(Date crateDate) {
        this.crateDate = crateDate;
    }
    public Date getCalculateDate() {
        return calculateDate;
    }
    public void setCalculateDate(Date calculateDate) {
        this.calculateDate = calculateDate;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getContractCost() {
        return contractCost;
    }
    public void setContractCost(String contractCost) {
        this.contractCost = contractCost;
    }
}
