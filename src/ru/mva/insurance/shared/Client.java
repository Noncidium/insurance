package ru.mva.insurance.shared;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name="clients")
@NamedQueries({
    @NamedQuery(name="Client.getAll", query="SELECT i FROM Client i")
})

public class Client extends Model {

	@NotBlank(message = "Поле имя обязательно к заполнению")
	@Pattern(regexp = "[а-я-А-Я]*", message = "Имя должно состоять только из русских букв")
	private String firstName;
	@NotBlank(message = "Поле фамилия обязательно к заполнению")
	@Pattern(regexp = "[а-я-А-Я]*", message = "Поле фамилия должно состоять только из русских букв")
	private String lastName;
	@Pattern(regexp = "[а-я-А-Я]*", message = "Поле отчество должно состоять только из русских букв")
	private String middleName;
	@Past(message = "Клиент из будущего?")
	private Date birthDay;
	@Size(min=4, max=4, message = "Поле серия паспорта должно состоять из 4 цифр")
	private String passportSeries;
	@Size(min=6, max=6, message = "Поле номер паспорта должно состоять из 6 цифр")
	private String passportNumber;

	private static final long serialVersionUID = 1L;

	public Client() {
	}

	public String getFirstName() {
		return this.firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public Date getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}
	public String getPassportSeries() {
		return passportSeries;
	}
	public void setPassportSeries(String passportSeries) {
		this.passportSeries = passportSeries;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	@Override
	public String toString() {
		return lastName + " " + firstName + " " + middleName;
	}
}
