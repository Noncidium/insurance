package ru.mva.insurance.shared;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="real_estate")
@NamedQueries({
    @NamedQuery(name="RealEstate.getAll",
                query="SELECT i FROM RealEstate i")
})

public class RealEstate extends Model {

    @NotBlank(message = "Поле государство обязательно к заполнению")
	private String country;

	private String zipCode;

    @NotBlank(message = "Поле республика, край, область обязательно к заполнению")
    private String region;

    private String district;

    @NotBlank(message = "Поле населенный пункт обязательно к заполнению")
    private String settlement;

    @NotBlank(message = "Поле улица обязательно к заполнению")
    private String street;

    @Pattern(regexp = "[0-9]*", message = "Поле дом должно состоять из цифр")
    private String house;

    private String housing;

    private String building;

    @Pattern(regexp = "[0-9]*", message = "Поле квартира должно состоять из цифр")
    @NotBlank(message = "Номер квартиры обязателен к заполнению")
    private String apartment;

    private String propertyType;

    private Integer yearBuilt;

    private Double area;


	private static final long serialVersionUID = 1L;

	public RealEstate() {
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }
    public void setDistrict(String district) {
        this.district = district;
    }

    public String getHouse() {
        return house;
    }
    public void setHouse(String house) {
        this.house = house;
    }

    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    public String getSettlement() {
        return settlement;
    }
    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }

    public String getHousing() {
        return housing;
    }
    public void setHousing(String housing) {
        this.housing = housing;
    }

    public String getBuilding() {
        return building;
    }
    public void setBuilding(String building) {
        this.building = building;
    }

    public String getApartment() {
        return apartment;
    }
    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getPropertyType() {
        return propertyType;
    }
    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public Integer getYearBuilt() {
        return yearBuilt;
    }
    public void setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public Double getArea() {
        return area;
    }
    public void setArea(Double area) {
        this.area = area;
    }
}
